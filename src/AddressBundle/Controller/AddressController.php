<?php

namespace AddressBundle\Controller;

use AddressBundle\Entities\Address;
use AddressBundle\Repositories\AddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AddressController extends Controller
{
    const MIN = 0.65;
    const MAX = 1;
    const MINST = 0.8;

    /** @var AddressRepository */
    private $addressRepository;

    public function __construct()
    {
        $this->addressRepository = new AddressRepository();
    }

    public function indexAction()
    {
        $addresses = $this->getAllData();

        return $this->render('@Address/Default/addresses.html.twig',
            [
                'addresses' => $addresses,
            ]
        );
    }

    public function scenarioTwoCsvAction()
    {
        error_reporting(E_ALL);
        $response = new StreamedResponse(function () {
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, [
                'Org_ID',
                'Corp_ID',
                'Name',
                'ORG_TYPE',
                'REGION',
                'City',
                'Street',
                'House',
                '70%name + ORG_Type +Address description',
                '70%name + ORG_Type +Address Org ids',
                '65%name + ORG_Type +Address80% description',
                '65%name + ORG_Type +Address80% Org ids'
            ], ';');

            foreach ($this->getDataScenario2() as $address) {
                try {
                    fputcsv(
                        $handle,
                        [
                            $address->getOrgId(),
                            $address->getCorpId(),
                            $address->getName(),
                            $address->getOrgType(),
                            $address->getRegion(),
                            $address->getCity(),
                            $address->getStreet(),
                            $address->getHouse(),
                            $this->getDuplicateDescriptionScenario1($address),
                            $this->getDuplicateIdString($address->getSimilarAddressesScenario1()),
                            $this->getDuplicateDescriptionScenario2($address),
                            $this->getDuplicateIdString($address->getSimilarAddressesScenario2()),
                        ],
                        ';'
                    );
                } catch (\Exception $ex) {
                    fputcsv(
                        $handle,
                        [
                            $ex->getMessage(),
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                        ],
                        ';'
                    );
                }
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="duplicate_addresses.csv"');

        return $response;
    }

    private function getDuplicateIdString(array $address)
    {
        $ids = [];
        foreach ($address as $similarAddress) {
            $ids[] = $similarAddress->getOrgId();
        }

        return implode(', ', $ids) ?? '';
    }

    private function getDuplicateDescriptionScenario1(Address $address): string
    {
        $duplicateDescriptionString = "";
        if (is_array($address->getSimilarAddressesScenario1())) {
            foreach ($address->getSimilarAddressesScenario1() as $similarAddress) {
                $duplicateDescriptionString .= "Name: " .
                    $similarAddress->getChangedName() .
                    ". Org id: " . $similarAddress->getOrgId() .
                    ". Match(%): " . (round($similarAddress->getNamePercentage(), 2)) * 100 . "\t\n\r";
            }
        } else {
            return 'not an array';
        }

        return $duplicateDescriptionString;
    }

    private function getDuplicateDescriptionScenario2(Address $address): string
    {
        $duplicateDescriptionString = "";
        if (is_array($address->getSimilarAddressesScenario2())) {
            foreach ($address->getSimilarAddressesScenario2() as $similarAddress) {
                $duplicateDescriptionString .=
                    "Name: " . $similarAddress->getChangedName() .
                    " Street: " . $similarAddress->getStreet() .
                    ". Org id: " . $similarAddress->getOrgId() .
                    ". Match street(%): " . (round($similarAddress->getStreetPercentage(), 2)) * 100 .
                    ". Match name(%): " . (round($similarAddress->getNamePercentage(), 2)) * 100 . "\t\n\r";
            }
        } else {
            return 'not an array';
        }

        return $duplicateDescriptionString;
    }

    public function scenarioTwoAction()
    {
        $addresses = $this->getDataScenario2();

        return $this->render('@Address/Default/addresses.html.twig',
            [
                'addresses' => $addresses,
            ]
        );
    }

    /**
     * @return Address[]
     */
    private function getAllData(): array
    {
        $addresses = $this->addressRepository->getAllAddresses();

        foreach ($addresses as $address) {
            $possibleAddresses = $this->addressRepository->getPossibleAddresses($address);

            foreach ($possibleAddresses as $possibleAddress) {
                $levensteinDistanceStreet = $this->compare($address->getStreet(), $possibleAddress->getStreet());
                $levensteinDistanceName = $this->compare($address->getChangedName(), $possibleAddress->getChangedName());

                if ($levensteinDistanceStreet >= self::MINST && $levensteinDistanceName >= self::MIN) {
                    $possibleAddress->setStreetPercentage($levensteinDistanceStreet);
                    $possibleAddress->setNamePercentage($levensteinDistanceName);
                    $address->addSimilarAddressScenario2($possibleAddress);
                }
            }
        }

        return $addresses;
    }

    //name>70%+org_type100%+adress100%
    private function getDataScenario2()
    {
        $addresses = $this->addressRepository->getAllAddresses();

        foreach ($addresses as $address) {
            $possibleAddresses = $this->addressRepository->getPossibleAddressesScenario2($address);

            foreach ($possibleAddresses as $possibleAddress) {

                $percentage = $this->compare($address->getChangedName(), $possibleAddress->getChangedName());

                if ($percentage >= self::MIN and $percentage <= self::MAX) {
                    $possibleAddress->setNamePercentage($percentage);
                    $address->addSimilarAddressScenario1($possibleAddress);
                }
            }

            $possibleAddresses = $this->addressRepository->getPossibleAddresses($address);

            foreach ($possibleAddresses as $possibleAddress) {
                $levensteinDistanceStreet = $this->compare($address->getStreet(), $possibleAddress->getStreet());
                $levensteinDistanceName = $this->compare($address->getChangedName(), $possibleAddress->getChangedName());

                if ($levensteinDistanceStreet >= self::MINST && $levensteinDistanceName >= self::MIN) {
                    $possibleAddress->setStreetPercentage($levensteinDistanceStreet);
                    $possibleAddress->setNamePercentage($levensteinDistanceName);
                    $address->addSimilarAddressScenario2($possibleAddress);
                }
            }
        }

        return $addresses;
    }

    private function compare($strBase, $strCompareTo)
    {
        if (!$strBase || !$strCompareTo) {
            return 0;
        }
        try {
            $levensteinDistance = levenshtein($strBase, $strCompareTo);
            if (strlen($strBase) >= strlen($strCompareTo)) {
                $streetLn = strlen($strBase);
            } else {
                $streetLn = strlen($strCompareTo);
            }
            if ($streetLn !== 0) {
                $percentage = (1 - ($levensteinDistance / $streetLn));
            } else {
                $percentage = 1;
            }
        } catch(\Exception $ex) {
            return 0;
        }

        return $percentage;
    }
}
