<?php

namespace AddressBundle\Repositories;

use AddressBundle\Entities\Address;

class AddressRepository
{
    const HOST = 'localhost';
    const PORT = 5432;
    const DB_NAME = 'postgres';
    const USER = 'postgres';
    const PASSWORD = '101010';

    private $connection;

    public function __construct()
    {
        $this->connection = pg_connect(
            sprintf(
                "host=%s port=%s dbname=%s user=%s password=%s",
                self::HOST,
                self::PORT,
                self::DB_NAME,
                self::USER,
                self::PASSWORD
            )
        );
    }

    /**
     * @param array $data
     * @return Address
     */
    private function fromArrayToEntity(array $data): Address
    {
        return new Address(
            $data['corp_id'],
            $data['street'],
            $data['house'],
            $data['city'],
            $data['org_id'],
            $data['name'],
            $data['org_type'],
            $data['address_id'],
            $data['region'],
            $data['changedname']

        );
    }

    /**
     * @param Address $address
     * @return Address[]
     */
    public function getPossibleAddresses(Address $address): array
    {
        $query = sprintf(
            "select org_id, corp_id, street,org_type,house,city,address_id,region,name,  LOWER (replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(name, '№', ''), ' № ', ' '), ' ЗК', ' '), ' СП', ' '), ' ФП', ' '), ' ФЛП', ' '), ' СВ', ' '), ' АО', ' '), ' НН ', ' '), ' НН', ''), '.ИП', ''), ' ИП', ''), ' ЧП', ''), ' ЗАО', ''), ' ПАО', ''), ' ОАО', ''), 'Аптечный пункт ', ''), ' ООО ', ' '), ' ООО', ''), 'Аптека ', '')) AS changedname from address where house = '%s' and city = '%s' and corp_id = '%s' AND org_id != '%s' ",
            $address->getHouse(),
            $address->getCity(),
            $address->getCorpId(),
            $address->getOrgId()

        );
        $data = pg_query($this->connection, $query);

        $possibleAddresses = [];

        while ($possibleAddress = pg_fetch_array($data, null, PGSQL_ASSOC)) {
            $possibleAddresses[] = $this->fromArrayToEntity($possibleAddress);
        }

        return $possibleAddresses;
    }


    /**
     * @param Address $address
     * @return Address[]
     */
    public function getPossibleAddressesScenario2(Address $address): array
    {
        $query = sprintf(
            "select org_id, corp_id, street,org_type,house,city,address_id,region,name,
            LOWER (replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(name, '№', ''), ' № ', ' '), ' ЗК', ' '), ' СП', ' '), ' ФП', ' '), ' ФЛП', ' '), ' СВ', ' '), ' АО', ' '), ' НН ', ' '), ' НН', ''), '.ИП', ''), ' ИП', ''), ' ЧП', ''), ' ЗАО', ''), ' ПАО', ''), ' ОАО', ''), 'Аптечный пункт ', ''), ' ООО ', ' '), ' ООО', ''), 'Аптека ', '')) AS changedname from address where org_type = '%s' and region = '%s' and city = '%s' and street='%s' and house='%s' AND org_id != '%s'",
            $address->getOrgType(),
            $address->getRegion(),
            $address->getCity(),
            $address->getStreet(),
            $address->getHouse(),
            $address->getOrgId()

        );
        $data = pg_query($this->connection, $query);

        $possibleAddresses = [];

        while ($possibleAddress = pg_fetch_array($data, null, PGSQL_ASSOC)) {
            $possibleAddresses[] = $this->fromArrayToEntity($possibleAddress);
        }

        return $possibleAddresses;
    }

    /**
     * @return Address[]
     */
    public function getAllAddresses(): array
    {
        $data = pg_query($this->connection, "select org_id, corp_id, street,org_type,house,city,address_id,region,name, LOWER (replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(name, '№', ''), ' № ', ' '), ' ЗК', ' '), ' СП', ' '), ' ФП', ' '), ' ФЛП', ' '), ' СВ', ' '), ' АО', ' '), ' НН ', ' '), ' НН', ''), '.ИП', ''), ' ИП', ''), ' ЧП', ''), ' ЗАО', ''), ' ПАО', ''), ' ОАО', ''), 'Аптечный пункт ', ''), ' ООО ', ' '), ' ООО', ''), 'Аптека ', ''))  AS changedname from address WHERE org_type = 'Аптека' OR org_type = 'Аптечный пункт' OR org_type = 'Аптечный киоск' AND org_status != 'Дубль'");
        $addresses = [];

        while ($address = pg_fetch_array($data, null, PGSQL_ASSOC)) {
            $addresses[] = $this->fromArrayToEntity($address);
        }

        return $addresses;
    }
}