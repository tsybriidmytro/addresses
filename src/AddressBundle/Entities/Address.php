<?php

namespace AddressBundle\Entities;

class Address
{
    /** @var int */
    private $corpId;
    /** @var string */
    private $street;
    /** @var int */
    private $house;
    /** @var string */
    private $city;
    /** @var Address[] */
    private $similarAddressesScenario1 = [];
    /** @var Address[] */
    private $similarAddressesScenario2 = [];
    /** @var int */
    private $orgId;
    /** @var float */
    private $percentage;
    /** @var string */
    private $name;
    /** @var string */
    private $orgType;
    /** @var string */
    private $address;
    /** @var string */
    private $region;
    /** @var string */
    private $changedName;
    /** @var float|null */
    private $streetPercentage;
    /** @var float|null */
    private $namePercentage;

    /**
     * Address constructor.
     * @param $corpId
     * @param $street
     * @param $house
     * @param $city
     * @param $orgId
     * @param $name
     * @param $orgType
     * @param $address
     * @param $region
     * @param $changedName
     */
    public function __construct(
        $corpId,
        $street,
        $house,
        $city,
        $orgId,
        $name,
        $orgType,
        $address,
        $region,
        $changedName
    )

    {
        $this->corpId = $corpId;
        $this->street = $street;
        $this->house = $house;
        $this->city = $city;
        $this->orgId = $orgId;
        $this->name = $name;
        $this->orgType = $orgType;
        $this->address = $address;
        $this->region = $region;
        $this->changedName = $changedName;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function getOrgType(): string
    {
        return $this->orgType;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCorpId(): string
    {
        return $this->corpId;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getHouse(): string
    {
        return $this->house;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function addSimilarAddressScenario1(Address $address): Address
    {
        $this->similarAddressesScenario1[] = $address;

        return $this;
    }

    /**
     * @return Address[]|null
     */
    public function getSimilarAddressesScenario1()
    {
        return $this->similarAddressesScenario1;
    }

    public function addSimilarAddressScenario2(Address $address): Address
    {
        $this->similarAddressesScenario2[] = $address;

        return $this;
    }

    /**
     * @return Address[]|null
     */
    public function getSimilarAddressesScenario2()
    {
        return $this->similarAddressesScenario2;
    }

    public function getOrgId(): string
    {
        return $this->orgId;
    }

    public function getPercentage(): string
    {
        return $this->percentage ?? '';
    }

    public function setPercentage(float $percentage): Address
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getPercentagestr(): float
    {
        return $this->percentage;
    }

    public function setPercentagestr(float $percentagestr): Address
    {
        $this->percentagestr = $percentagestr;

        return $this;
    }

    public function getChangedName()
    {
        return $this->changedName;
    }


    /**
     * @return float|null
     */
    public function getStreetPercentage()
    {
        return $this->streetPercentage ?? 0;
    }

    public function setStreetPercentage(float $streetPercentage)
    {
        $this->streetPercentage = $streetPercentage;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getNamePercentage()
    {
        return $this->namePercentage;
    }

    public function setNamePercentage(float $namePercentage)
    {
        $this->namePercentage = $namePercentage;
    }
}